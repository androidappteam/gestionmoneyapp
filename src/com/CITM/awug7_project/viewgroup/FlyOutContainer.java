package com.CITM.awug7_project.viewgroup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.Scroller;

@SuppressLint("NewApi")
public class FlyOutContainer extends LinearLayout {

	// dos grupos de elementos en la vista: el men� y el contenido.
	private View menu;
	private View content;

	// Margen del men�: p�xeles que se ven del contenido cuando el men� est� abierto.
	protected static final int menuMargin = 150;
	
	//4 estados del men�, seg�n se le asigna, hace unas cosas u otras
	public enum MenuState {
		CLOSED, OPEN, CLOSING, OPENING
	};

	// currentContentOffset nos dice el progreso de desplazamiento del menu. 
	//Cuando est� totalmente escondido ser� 0. Cuando est� totelmente desplegado ser� igual a la anchura del men�.
	protected int currentContentOffset = 0;
	//MenuState contiene el actual estado del men�
	protected MenuState menuCurrentState = MenuState.CLOSED;
	
	//Objetos para la animaci�n del men�.
	protected Scroller menuAnimationScroller = new Scroller(this.getContext(),
		new SmoothInterpolator());
	protected Runnable menuAnimationRunnable = new AnimationRunnable();
	protected Handler menuAnimationHandler = new Handler();

	//Constantes para la animaci�n. 
	private static final int menuAnimationDuration = 1000;
	private static final int menuAnimationPollingInterval = 16;
	
	//Constructores
	public FlyOutContainer(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public FlyOutContainer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FlyOutContainer(Context context) {
		super(context);
	}

	//funcion onAttachedToWindow. Nos aseguramos que ahora las vistas ya et�n construidas. En el constructor podr�an no estarlo.
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		this.menu = this.getChildAt(0); //asignamos el primer hijo de window a la variable menu
		this.content = this.getChildAt(1); //y el segundo a content
		
		//Ocultamos el contenido de menu para versiones antiguas de Android, en las que anque la capa est� tapada, la intenta dibujar. 
		this.menu.setVisibility(View.GONE); 
	}
	
	
	/*Posicionamos el menu y el contenido en la pantalla
	Pasamos 5 par�metros:
	- changed: ser� true si la medida de FlyoutContainer ha cambiado desde la ultima vez que se llam�.
	- left, top, right, bottom: extremos del FlyoutContainer
	*/
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		if (changed)
			//Si han cambiado las medidas, las calculamos
			this.calculateChildDimensions();
		//posicionamos el menu en la izquierda de la pantalla
		this.menu.layout(left, top, right - menuMargin, bottom);
		
		//la posici�n left y right de content depende de currentContentOffset
		this.content.layout(left + this.currentContentOffset, top, right + this.currentContentOffset, bottom);

	}
	
	//funci�n que pliega o despliega el men�. Dependiendo del menucurrentState, hace una cosa u otra.
	public void toggleMenu() {
		switch (this.menuCurrentState) {
		case CLOSED:
			this.menuCurrentState = MenuState.OPENING;
			this.menu.setVisibility(View.VISIBLE);
			//llamamos la funcion menuAnimationScroller con los par�metros
			this.menuAnimationScroller.startScroll(0, 0, this.getMenuWidth(), 0, menuAnimationDuration);
			break;
		case OPEN:
			this.menuCurrentState = MenuState.CLOSING;
			this.menuAnimationScroller.startScroll(this.currentContentOffset, 0, -this.currentContentOffset, 0, menuAnimationDuration);
			break;
		default:
			return;
		}

		this.menuAnimationHandler.postDelayed(this.menuAnimationRunnable, menuAnimationPollingInterval);
	}
	
	//funci�n que retorna la anchura del men�
	private int getMenuWidth() {
		return this.menu.getLayoutParams().width;
	}
	
	//funci�n que define las dimensiones de content y del menu.
	private void calculateChildDimensions() {
		
		//content: ocupa toda la pantalla en vertical y horizontal.
		this.content.getLayoutParams().height = this.getHeight();
		this.content.getLayoutParams().width = this.getWidth();

		//menu: ocupa toda la pantalla en vertical y toda - menuMargin en horizontal.
		this.menu.getLayoutParams().width = this.getWidth() - menuMargin;
		this.menu.getLayoutParams().height = this.getHeight();
	}
	
	protected class AnimationRunnable implements Runnable {
		
		@Override
		public void run() {
			boolean isAnimationOngoing = FlyOutContainer.this.menuAnimationScroller.computeScrollOffset();
			
			FlyOutContainer.this.adjustContentPosition(isAnimationOngoing);
		}
	}

	public void adjustContentPosition(boolean isAnimationOngoing) {
		int scrollerOffset = this.menuAnimationScroller.getCurrX();
		
		this.content.offsetLeftAndRight(scrollerOffset - this.currentContentOffset);
		
		this.currentContentOffset = scrollerOffset;
		
		this.invalidate();
		
		if(isAnimationOngoing)
			this.menuAnimationHandler.postDelayed(this.menuAnimationRunnable, menuAnimationPollingInterval);
		else
			this.onMenuTransitionComplete();
		
	}
	
	protected class SmoothInterpolator implements Interpolator {
	
		@Override
		public float getInterpolation(float t) {
			return (float) Math.pow(t - 1, 5) +1;
		}
	}

	private void onMenuTransitionComplete() {
		
		switch(this.menuCurrentState){
		case OPENING:
			this.menuCurrentState = MenuState.OPEN;
			break;
		case CLOSING:
			this.menuCurrentState = MenuState.CLOSED;
			break;
		default:
			return;
			
		}
		
	}

}
