package com.CITM.awug7_project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsoluteLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Inform extends ActionBarActivity {
	SQLiteDatabase db;
	DBHelper dbhelper;
	int categoria = 0;
	// TextView txtTitulo;
	boolean filters = true;
	boolean filter_active = false;
	Integer cat_clicked;
	View last_clicked;
	Boolean costT_ingressF = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.inform);
		/*
		 * txtTitulo = (TextView) findViewById(R.id.titulo);
		 * txtTitulo.bringToFront();
		 * 
		 * if (categoria == 0) { txtTitulo.setText("Todos"); }
		 */

		dbhelper = new DBHelper(this);

		LinearLayout fragContainer = (LinearLayout) findViewById(R.id.inform_layout);
		List<InformData> list = dbhelper.getCompleteInform();
		for (InformData informListItem : list) {
			Fragment frg = new InformContent();
			Bundle bundle = new Bundle();
			bundle.putInt("id", informListItem.getId());
			bundle.putInt("category", informListItem.getCategory());
			bundle.putString("type", informListItem.getType());
			bundle.putString("concept", informListItem.getConcept());
			bundle.putFloat("cost", informListItem.getCost());
			bundle.putString("date", informListItem.getDate());
			frg.setArguments(bundle);
			FragmentManager fragMan = getSupportFragmentManager();
			FragmentTransaction fragTransaction = fragMan.beginTransaction();
			LinearLayout ll = new LinearLayout(this);
			ll.setOrientation(LinearLayout.VERTICAL);
			ll.setId(informListItem.getId());
			fragTransaction.add(ll.getId(), frg).commit();
			fragContainer.addView(ll);
		}
		TextView saldo_inf = (TextView) findViewById(R.id.saldo_inf);
		saldo_inf.setText("Saldo: " + dbhelper.getSaldo());
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.inform, menu); return true; }
	 */

	public void category(View v) { // --> Se ejecuta cuando clicamos a uno de
									// los botones de categor�a
		// Creamos un array de strings con el String Array de
		// values>strings>name=categorias
		last_clicked = v;
		String[] arrayCategorias = getResources().getStringArray(
				R.array.categorias);

		// asignamos a la variable menucategories el LinearLayout
		// id=menucategories
		LinearLayout menucategories = (LinearLayout) this
				.findViewById(R.id.menucategories);

		// cogemos la id del bot�n clicado (v), cogemos el �ltimo car�cter
		// despu�s de "y", lo convertimos a int y lo asignamos a la variable
		// categoria
		String IdAsString = v.getResources().getResourceName(v.getId());
		categoria = Integer.parseInt(IdAsString.substring(IdAsString
				.lastIndexOf("y") + 1));

		// cambiamos el text del textView txtTitulo por el string de
		// arrayCategorias en la posici�n [categoria-1]
		// txtTitulo.setText(arrayCategorias[categoria - 1]);
		getSupportActionBar().setTitle(arrayCategorias[categoria - 1]);
		// contamos los hijos del linearLayout menucategories y vamos pasando
		// por ellos
		int childcount = menucategories.getChildCount();
		for (int i = 0; i < childcount; i++) {
			View botoCategoria = menucategories.getChildAt(i);
			if (cat_clicked != null && cat_clicked == categoria) {
				botoCategoria.setBackgroundColor(Color.parseColor("#0A6BBE"));
			} else {
				if (v.getId() == botoCategoria.getId()) {

					// si coincide la id del bot�n clicado con la del hijo,
					// cambiamos el backgroundColor a verde
					botoCategoria.setBackgroundColor(Color
							.parseColor("#09cc8d"));
					last_clicked = botoCategoria;
				} else {

					// sino, lo dejamos al color azul
					botoCategoria.setBackgroundColor(Color
							.parseColor("#0A6BBE"));
				}
			}

		}
		informFilter(categoria, arrayCategorias);
	}

	private void informFilter(int categoria, String[] arrayCategorias) {
		LinearLayout fragContainer = (LinearLayout) findViewById(R.id.inform_layout);
		if (!filter_active || (filter_active && cat_clicked != categoria)) {
			Button filteringres = (Button) findViewById(R.id.filteringres);
			filteringres.setBackgroundColor(getResources().getColor(
					R.color.ingress));
			filter_active = true;
			cat_clicked = categoria;
			if (arrayCategorias[categoria - 1].equals("Todos")) {
				for (Integer i = 0; i < fragContainer.getChildCount(); i++) {
					View v = fragContainer.getChildAt(i);
					v.setVisibility(View.VISIBLE);
				}
			} else {
				for (Integer i = 0; i < fragContainer.getChildCount(); i++) {
					View v = fragContainer.getChildAt(i);
					TextView categorytextview = (TextView) v
							.findViewById(R.id.inform_category);
					if (arrayCategorias[categoria - 1].equals(categorytextview
							.getText())) {
						TextView typeview = (TextView) v
								.findViewById(R.id.inform_type);
						if (typeview.getText().toString().equals("ingress")) {

						} else {
							Log.v("AAAAAAAAAA", typeview.getText().toString());
							v.setVisibility(View.VISIBLE);
						}
					} else {
						v.setVisibility(View.GONE);
					}
				}
			}
		} else {
			filter_active = false;
			for (Integer i = 0; i < fragContainer.getChildCount(); i++) {
				View v = fragContainer.getChildAt(i);
				v.setVisibility(View.VISIBLE);
			}
			cat_clicked = null;
		}

	}

	public void informFiltro(View v) {
		String[] arrayCategorias = getResources().getStringArray(
				R.array.categorias);
		int filtro = v.getId();
		Button b = (Button) v;
		String buttonText = b.getText().toString();
		Log.v("filtro -> ", buttonText);
		Button filteringres = (Button) findViewById(R.id.filteringres);
		Button filtergastos = (Button) findViewById(R.id.filtergastos);
		LinearLayout fragContainer = (LinearLayout) findViewById(R.id.inform_layout);
		if (buttonText.equals("GASTOS")) {
			if (costT_ingressF == null || !costT_ingressF) {
				costT_ingressF = true;
				filtergastos.setBackgroundColor(getResources().getColor(
						R.color.subspending));
				filteringres.setBackgroundColor(getResources().getColor(
						R.color.ingress));

				for (Integer i = 0; i < fragContainer.getChildCount(); i++) {
					View vi = fragContainer.getChildAt(i);
					TextView typeview = (TextView) vi
							.findViewById(R.id.inform_type);

					if (typeview.getText().toString().equals("cost")) {
						if (filter_active) {
							TextView categorytextview = (TextView) vi
									.findViewById(R.id.inform_category);
							if (arrayCategorias[cat_clicked - 1]
									.equals(categorytextview.getText())) {
								vi.setVisibility(View.VISIBLE);
							} else {
								vi.setVisibility(View.GONE);
							}
						} else {
							vi.setVisibility(View.VISIBLE);
						}
					} else {
						vi.setVisibility(View.GONE);
					}
				}
			} else {
				costT_ingressF = null;
				filtergastos.setBackgroundColor(getResources().getColor(
						R.color.spending));
				for (Integer i = 0; i < fragContainer.getChildCount(); i++) {
					View vi = fragContainer.getChildAt(i);
					TextView typeview = (TextView) vi
							.findViewById(R.id.inform_type);
					vi.setVisibility(View.VISIBLE);
				}
			}
		}
		if (buttonText.equals("INGRESAR")) {
			if (costT_ingressF == null || costT_ingressF) {
				costT_ingressF = false;
				filteringres.setBackgroundColor(getResources().getColor(
						R.color.subingress));
				filtergastos.setBackgroundColor(getResources().getColor(
						R.color.spending));
				for (Integer i = 0; i < fragContainer.getChildCount(); i++) {
					View vi = fragContainer.getChildAt(i);

					TextView typeview = (TextView) vi
							.findViewById(R.id.inform_type);
					if (typeview.getText().toString().equals("ingress")) {
						vi.setVisibility(View.VISIBLE);
					} else {
						vi.setVisibility(View.GONE);
					}
				}
			} else {
				costT_ingressF = null;
				filteringres.setBackgroundColor(getResources().getColor(
						R.color.ingress));
				for (Integer i = 0; i < fragContainer.getChildCount(); i++) {
					View vi = fragContainer.getChildAt(i);
					TextView typeview = (TextView) vi
							.findViewById(R.id.inform_type);
					vi.setVisibility(View.VISIBLE);
				}
			}
			if (last_clicked != null) {
				last_clicked.setBackgroundColor(Color.parseColor("#0A6BBE"));
			}
			filter_active = false;
			cat_clicked = null;
		}
	}

	public void onofFilters(View v) {
		ImageButton btnonoffilters = (ImageButton) findViewById(R.id.btnonoffilters);
		final Animation slideUp = AnimationUtils.loadAnimation(this,
				R.anim.slide_up);
		final Animation slideDown = AnimationUtils.loadAnimation(this,
				R.anim.slide_down);
		LinearLayout filtros = (LinearLayout) findViewById(R.id.inform_filters);
		if (filters == false) {
			filtros.startAnimation(slideDown);
			filtros.setVisibility(LinearLayout.VISIBLE);
			btnonoffilters.setImageResource(R.drawable.flecha);
			Log.v("filters =", "" + filters);
			filters = true;
		} else if (filters == true) {
			filtros.startAnimation(slideUp);
			filtros.setVisibility(View.GONE);
			btnonoffilters.setImageResource(R.drawable.flecha2);
			Log.v("filters =", "" + filters);
			filters = false;
		}
	}

	private String getPackageId() {
		// TODO Auto-generated method stub
		return null;
	}

}
