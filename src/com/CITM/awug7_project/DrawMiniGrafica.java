package com.CITM.awug7_project;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DrawMiniGrafica extends View{
	DBHelper dbhelper = new DBHelper(getContext());
	
	Paint paint = new Paint();
	
    public DrawMiniGrafica(Context context) {
        super(context);
    }
    public DrawMiniGrafica(Context context, AttributeSet attrs){
    	super(context, attrs);
    }
    public DrawMiniGrafica(Context context, AttributeSet attrs, int defStyle){
    	super(context, attrs, defStyle);
    }

    @SuppressLint("ResourceAsColor")
	@Override
    public void onDraw(Canvas canvas) {
    	WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
    	int width = display.getWidth();  // deprecated
    	//int height = display.getHeight();
    	
    	int blau = getContext().getResources().getColor(R.color.background);
    	paint.setColor(getResources().getColor(R.color.text));
	    canvas.drawText("Ingresos totales: "+dbhelper.getTotalingresos(), 30, 40, paint);
	    canvas.drawText("Gastos totales: "+dbhelper.getTotalgastos(), 30, 80, paint);
	    canvas.drawRect(30, 100, width-30, 102, paint );
	    canvas.drawText("Saldo: "+dbhelper.getSaldo(), 30, 142, paint);
	    
	    
    	
    	
    	int verd = getContext().getResources().getColor(R.color.ingress);
    	int vermell = getContext().getResources().getColor(R.color.spending);
    	
    	int blaufondo = getContext().getResources().getColor(R.color.menubutback);
		String[] arrayCategorias = getResources().getStringArray(R.array.categorias);

    	super.onDraw(canvas);
    	
    	
        paint.setStrokeWidth(0);
        paint.setColor(verd);paint.setColor(Color.WHITE);
        /*canvas.drawRect(width/2, 15, width-15, 75, paint );
        paint.setColor(vermell);
        canvas.drawRect(15, 15, width/2, 75, paint );*/
        int scaledSize = getResources().getDimensionPixelSize(R.dimen.FontSizeGrafica);
        paint.setTextSize(scaledSize);
        int categoria = MainActivity.categoria;
        //canvas.drawText("categoria: "+categoria+" "+arrayCategorias[categoria], 30, 100, paint);
        paint.setColor(blau);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(width/2, width*3/5 , 135, paint);

        //canvas.drawText("Gastos por categoria: ", 30, 50, paint);
        //int[] colorquesitos = new int[]{Color.YELLOW,Color.BLUE,Color.RED,Color.GREEN, Color.MAGENTA};
        String[] nombrecategoria= new String[]{"Ocio","Transporte","Hogar","Alimentación","Otros"};
        
        float iniciquesito=0;
        for(int i=0;i<5;i++){
        	if(i==(categoria-1)){
        		Log.v("AAAAAAA", nombrecategoria[categoria]);
	        	paint.setColor(getResources().getColor(R.color.text));
	        	float percentage=dbhelper.getGastosCategoria(i)*100.0f/dbhelper.getTotalgastos();
		        String percentmostrar = String.format("%.1f", percentage);
		        float grausquesito=(float)3.6*percentage;	        
		        
		        RectF rectFa = new RectF(width/2-130, width*3/5-130, width/2+130, width*3/5+130);
		        canvas.drawArc (rectFa, 0, grausquesito, true, paint);
		        paint.setColor(blaufondo);
                canvas.drawCircle(width/2, width*3/5 , 100, paint);
                paint.setColor(getResources().getColor(R.color.text));
		        canvas.drawText(""+percentmostrar+"% ("+dbhelper.getGastosCategoria(i+1)+"€)", width/2-60, width*3/5, paint);
		        iniciquesito=iniciquesito+grausquesito;
		        break;
        	}else{
        		paint.setColor(blaufondo);
                canvas.drawCircle(width/2, width*3/5 , 100, paint);
                if(i==4){
                	break;
                }
        	}
        }
        paint.setColor(blaufondo);    
        
        
    }
	private String toString(int categoria) {
		// TODO Auto-generated method stub
		return null;
	}

}
