package com.CITM.awug7_project;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class Graficas extends ActionBarActivity {
	SQLiteDatabase db;
	DBHelper dbhelper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.graficas);

		dbhelper = new DBHelper(this);

	    TextView i = (TextView)findViewById(R.id.total_ingresos); 
	    i.setText("Total ingresos: "+dbhelper.getTotalingresos());
	    
	    TextView g = (TextView)findViewById(R.id.total_gastos); 
	    g.setText("Total gastos: "+dbhelper.getTotalgastos());
	    
	    int saldo =dbhelper.getSaldo();
	    TextView t = (TextView)findViewById(R.id.saldo_total); 
	    t.setText("Saldo total: "+saldo);
	}
}

