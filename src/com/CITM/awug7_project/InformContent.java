package com.CITM.awug7_project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class InformContent extends Fragment {

	private ViewSwitcher switcher;
	private Integer id;
	private String typeView;
	private String dateText;
	private String conceptText;
	private Integer categoryText;
	private float costText;
	LinearLayout ll;
	DBHelper dbhelper;
	TextView date;
	TextView type;
	TextView category;
	TextView concept;
	TextView cost;
	EditText dateEdit;
	EditText conceptEdit;
	EditText costEdit;
	Button filteringress;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		ArrayAdapter<String> adapter;
		String[] arrayCategorias = getResources().getStringArray(
				R.array.categorias);


		View view = inflater.inflate(R.layout.inform_content, container, false);
		switcher = (ViewSwitcher) view.findViewById(R.id.profileSwitcher);
		Bundle bundle = this.getArguments();
		id = bundle.getInt("id");
		typeView = bundle.getString("type");
		dateText = bundle.getString("date");
		conceptText = bundle.getString("concept");
		categoryText = bundle.getInt("category");
		costText = bundle.getFloat("cost");
		View v = (View) view.findViewById(R.id.view_background);
		if (typeView.equalsIgnoreCase("ingress")) {
			v.setBackgroundColor(getResources().getColor(R.color.ingress));
		} else {
			v.setBackgroundColor(getResources().getColor(R.color.spending));
		}
		
		date = (TextView) view.findViewById(R.id.inform_date_ind);
		date.setText("" + dateText);
		type = (TextView) view.findViewById(R.id.inform_type);
		type.setText("" + typeView);
		category = (TextView) view.findViewById(R.id.inform_category);
		category.setText(arrayCategorias[categoryText]);
		if (typeView.equalsIgnoreCase("ingress")) {
			category.setVisibility(View.GONE);
		}
		concept = (TextView) view.findViewById(R.id.inform_concept);
		concept.setText("" + conceptText);
		cost = (TextView) view.findViewById(R.id.inform_cost);
		cost.setText("" + costText);
		ll = (LinearLayout) view.findViewById(R.id.content_view);
		ll.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View v) {
				dateEdit = (EditText) v.findViewById(R.id.inform_date_ind_mod);
				dateEdit = (EditText) v
						.findViewById(R.id.inform_date_ind_mod);
				dateEdit.setHint(date.getText());
				dateEdit.setText(date.getText());
				conceptEdit = (EditText) v
						.findViewById(R.id.inform_concept_mod);
				conceptEdit.setHint(concept.getText());
				conceptEdit.setText(concept.getText());
				costEdit = (EditText) v.findViewById(R.id.inform_cost_mod);
				costEdit.setHint(cost.getText());
				costEdit.setText(cost.getText());
				final LinearLayout ll = (LinearLayout) v
						.findViewById(R.id.content_view);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) ll
						.getLayoutParams();
				params.height = 300;
				ll.setLayoutParams(params);
				ImageButton update = (ImageButton) v
						.findViewById(R.id.button_modupdate_inform);
				//LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) 
				ll.getLayoutParams();
				params.height = 300;
				ll.setLayoutParams(params);
				//Button update=(Button) v.findViewById(R.id.button_modupdate_inform);
				update.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						String dateText2 = null;
						String conceptText2 = null;
						Float costText2 = null;
						if (!dateEdit.getText().equals("")
								|| !dateEdit.getText().equals(
										dateEdit.getHint())) {
							dateText2 = dateEdit.getText().toString();
						} else {
							String dateText = dateEdit.getHint().toString();
						}
						;
						if (conceptEdit.getText() != null
								|| !conceptEdit.getText().equals(
										conceptEdit.getHint())) {
							conceptText2 = conceptEdit.getText().toString();
						} else {
							String conceptText = conceptEdit.getHint()
									.toString();
						}
						;
						if (costEdit.getText() != null
								|| !costEdit.getText().equals(
										costEdit.getHint())) {
							costText2 = Float.parseFloat(costEdit.getText()
									.toString());
						} else {
							String costText = costEdit.getHint().toString();
						}
						;

						InformData informdata = new InformData(typeView,
								categoryText, conceptText2, costText2,
								dateText2);
						informdata.setId(id);
						dbhelper = new DBHelper(v.getContext());
						dbhelper.updateInform(informdata);
						LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) ll
								.getLayoutParams();
						if(!dateEdit.getText().equals("") || !dateEdit.getText().equals(dateEdit.getHint())){
							dateText2=dateEdit.getText().toString();
						}else{String dateText=dateEdit.getHint().toString();};
						if(conceptEdit.getText()!=null || !conceptEdit.getText().equals(conceptEdit.getHint())){
							conceptText2=conceptEdit.getText().toString();
						}else{String conceptText=conceptEdit.getHint().toString();};
						if(costEdit.getText()!=null || !costEdit.getText().equals(costEdit.getHint())){
							costText2=Float.parseFloat(costEdit.getText().toString());
						}else{String costText=costEdit.getHint().toString();};
						
						//InformData informdata=new InformData(typeView, categoryText, conceptText2, costText2, dateText2);
						informdata.setId(id);
						dbhelper = new DBHelper(v.getContext());
						dbhelper.updateInform(informdata);
						//LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) 
						ll.getLayoutParams();
						params.height = 160;
						ll.setLayoutParams(params);
						date.setText(dateText2);
						concept.setText(conceptText2);
						cost.setText(costText2.toString());
						switcher.showNext();
					}
				});
				ImageButton cancel = (ImageButton) v
						.findViewById(R.id.button_modcancel_inform);
				cancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) ll
								.getLayoutParams();
						params.height = 160;
						ll.setLayoutParams(params);
						switcher.showNext();
					}
				});
				ImageButton delete = (ImageButton) v
						.findViewById(R.id.button_moddelete_inform);
				delete.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						InformData informdata = new InformData(typeView,
								categoryText, conceptText, costText,
								dateText);
						informdata.setId(id);
						dbhelper = new DBHelper(v.getContext());
						dbhelper.deleteInform(informdata);
						deleteThis();
					}

					
				});
				switcher.showNext();
				return false;
			}
		});
		return view;
	}
	private void deleteThis() {
		getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		
	}
}
