package com.CITM.awug7_project;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class DrawGrafica extends View {
	DBHelper dbhelper = new DBHelper(getContext());

	Paint paint = new Paint();

	public DrawGrafica(Context context) {
		super(context);
	}

	public DrawGrafica(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DrawGrafica(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public void onDraw(Canvas canvas) {
		final int totalgastos = dbhelper.getTotalgastos();
		WindowManager wm = (WindowManager) getContext().getSystemService(
				Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		// int height = display.getHeight();
		int verd = getContext().getResources().getColor(R.color.ingress);
		int vermell = getContext().getResources().getColor(R.color.spending);
		int blau = getContext().getResources().getColor(R.color.background);
		super.onDraw(canvas);
		paint.setStrokeWidth(0);
		paint.setColor(verd);
		Float gastos = (float) dbhelper.getTotalgastos();
		Float ingresos = (float) dbhelper.getTotalingresos();
		Float porcentaje = gastos / ingresos;

		canvas.drawRect(15, 15, width - 15, 75, paint);
		paint.setColor(vermell);
		if (gastos == 0) {

		} else if (porcentaje == 0) {
			canvas.drawRect(15, 15, (width - 30), 75, paint);
		} else {
			canvas.drawRect(15, 15, (width - 30) * porcentaje, 75, paint);
		}
		int scaledSize = getResources().getDimensionPixelSize(
				R.dimen.FontSizeGrafica);
		paint.setTextSize(scaledSize);

		paint.setColor(blau);
		paint.setStyle(Paint.Style.FILL);
		canvas.drawCircle(width / 2, width, 100, paint);
		paint.setColor(Color.WHITE);
		canvas.drawText("Gastos por categoria: ", 30, 110, paint);
		int[] colorquesitos = new int[] { Color.YELLOW, Color.GRAY, Color.RED,
				Color.GREEN, Color.WHITE };
		String[] nombrecategoria = new String[] { "Ocio", "Transporte",
				"Hogar", "Alimentación", "Otros" };
		float iniciquesito = 0;
		for (int i = 0; i < 5; i++) {
			String percentmostrar;
			float grausquesito=0;
			paint.setColor(colorquesitos[i]);
			if (dbhelper.getGastosCategoria(i) != 0) {
				float percentage = dbhelper.getGastosCategoria(i) * 100.0f
						/ dbhelper.getTotalgastos();
				percentmostrar = String.format("%.1f", percentage);
				grausquesito = (float) 3.6 * percentage;
				canvas.drawText(
						nombrecategoria[i] + ":   "
								+ dbhelper.getGastosCategoria(i) + "    ("
								+ percentmostrar + "%)", 30, 162 + (i * 50), paint);
			} else{
				percentmostrar = "0";
				canvas.drawText(
						nombrecategoria[i] + ":   "
								+ dbhelper.getGastosCategoria(i) + "    (0%)", 30,
						162 + (i * 50), paint);
			}

			RectF rectFa = new RectF(width / 2 - 90, width - 90,
					width / 2 + 90, width + 90);
			canvas.drawArc(rectFa, iniciquesito, grausquesito, true, paint);
			// Log.d(nombrecategoria[i],
			// "percentatge:"+percentage+" inici:"+iniciquesito+" graus:"+grausquesito+" color:"+colorquesitos[i]);
			iniciquesito = iniciquesito + grausquesito;
		}
		paint.setColor(blau);
		canvas.drawCircle(width / 2, width, 70, paint);

	}
}
