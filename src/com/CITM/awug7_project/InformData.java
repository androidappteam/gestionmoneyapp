package com.CITM.awug7_project;

public class InformData {
	private int id;
	private String type;
	private int category;
	private String concept;
	private float cost;
	private String date;

	public InformData(String type, int category, String concept, float cost, String date) {
		this.type = type;
		this.category= category;
		this.concept = concept;
		this.cost = cost;
		this.date = date;
	}

	public int getId() {
		return id;
	}
	public int getCategory() {
		return category;
	}
	public int setCategory(int category) {
		return category;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConcept() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {

		return "InformData [id=" + id + ", category=" + category +", concept=" + concept + ", cost="
				+ cost + ", date=" + date + "]";
		
	}

	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	public void setType(String type) {
		this.type=type;
	}

}
