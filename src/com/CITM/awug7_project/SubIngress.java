package com.CITM.awug7_project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SubIngress extends Fragment {
	DBHelper dbhelper;
	int category=MainActivity.categoria;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("info","Fragment subIngress");
		View view = inflater.inflate(R.layout.subingress, container, false);
		Button ingress = (Button) view.findViewById(R.id.make_ingress_button);
		ingress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditText concept = (EditText) getView().findViewById(R.id.conceptIngres);
				EditText cost = (EditText) getView().findViewById(R.id.quantityIngres);
				if (concept.getText() != null && cost.getText() != null) {
					String conceptText = concept.getText().toString();
					String costText = cost.getText().toString();
					float costValue = Float.parseFloat(costText);
					DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
					Date today = Calendar.getInstance().getTime();
					String reportDate = df.format(today);
					InformData informdata = new InformData("ingress", 0,
							conceptText, costValue, reportDate);
							//Toast para indicar el gasto
					Toast toast = Toast.makeText(v.getContext(), 
							R.string.toastIngress, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 150);
					toast.show();
					
					dbhelper = new DBHelper(v.getContext());
					dbhelper.addInform(informdata);
					concept.setText("");
					cost.setText("");
				}
			}
		});
		return view;
	}
}
