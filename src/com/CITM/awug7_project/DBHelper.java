package com.CITM.awug7_project;

import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "data.db";
	public static final String TABLE_NAME = "ingress_spend";
	public static final String ID = "id";
	public static final String CATEGORY = "category";
	public static final String TYPE = "type";
	public static final String CONCEPT = "concept";
	public static final String COST = "cost";
	public static final String DATE = "date";
	public static final int VERSION = 3;

	private final String createDB = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + CATEGORY + " INTEGER, "+ TYPE + " TEXT, "
			+ CONCEPT + " TEXT, " + COST + " TEXT, " + DATE + " TEXT);";
		
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(createDB);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int old_version, int new_version) {
		// TODO Auto-generated method stub

	}

	public void addInform(InformData informdata) {
		Log.d("addInform", informdata.toString());

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(CATEGORY, informdata.getType());
		values.put(TYPE, informdata.getCategory());
		values.put(CONCEPT, informdata.getConcept());
		values.put(COST, informdata.getCost());
		values.put(DATE, informdata.getDate());

		db.insert(TABLE_NAME, null, values);

		db.close();
	}

	public List<InformData> getCompleteInform() {
		List<InformData> cInfrom = new LinkedList<InformData>();

		String query = "SELECT  * FROM " + TABLE_NAME;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		InformData informdata = null;
		if (cursor.moveToFirst()) {
			do { //InformData(String type, int category, String concept, float cost, String date)
				informdata = new InformData(cursor.getString(1), cursor.getInt(2), cursor.getString(3), Float.parseFloat(cursor.getString(4)), cursor.getString(5));
				informdata.setId(Integer.parseInt(cursor.getString(0)));
				cInfrom.add(informdata);
			} while (cursor.moveToNext());
		}

		Log.d("getCompleteInform()", cInfrom.toString());
		db.close();
		return cInfrom;
	}
	int saldo;
	public int getSaldo() {
		SQLiteDatabase db = this.getWritableDatabase();
		 
		String sql = "SELECT SUM(COST) FROM "+TABLE_NAME+" WHERE "+ CATEGORY+" ='ingress'";
		
		long ingress = android.database.DatabaseUtils.longForQuery(db, sql, null);
		//Log.d("sql ->", sql+" //ingress== "+ingress);
		String sql2 = "SELECT SUM(COST) FROM "+TABLE_NAME+" WHERE "+ CATEGORY+"  ='cost'";
		long gastos = android.database.DatabaseUtils.longForQuery(db, sql2, null);
		//Log.d("sql ->", sql2+" //gastos== "+gastos);
		//Log.d("saldo -->", ""+ingress +"-"+gastos);
		saldo = (int) (ingress - gastos);
		db.close();
		return saldo;
	}
	public int getTotalingresos() {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "SELECT SUM(COST) FROM "+TABLE_NAME+" WHERE "+ CATEGORY+" ='ingress'";
		long ingress = android.database.DatabaseUtils.longForQuery(db, sql, null);
		db.close();
		return (int)ingress;
	}
	public int getTotalgastos() {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "SELECT SUM(COST) FROM "+TABLE_NAME+" WHERE "+ CATEGORY+" ='cost'";
		long gastos = android.database.DatabaseUtils.longForQuery(db, sql, null);
		db.close();
		return (int)gastos;
	}
	public int getGastosCategoria(int categoria) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "SELECT SUM(COST) FROM "+TABLE_NAME+" WHERE "+ CATEGORY+" ='cost' AND "+ TYPE+" ="+categoria;
		long gastoscategoria = android.database.DatabaseUtils.longForQuery(db, sql, null);
		db.close();
		return (int)gastoscategoria;
	}
	
	public int updateInform(InformData informdata) {

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(CATEGORY, informdata.getCategory());
		values.put(TYPE, informdata.getType());
		values.put(CONCEPT, informdata.getConcept());
		values.put(COST, informdata.getCost());
		values.put(DATE, informdata.getDate());

		int i = db.update(TABLE_NAME, values, ID + " = ?",
				new String[] { String.valueOf(informdata.getId()) });

		db.close();

		return i;

	}

	public void deleteInform(InformData informdata) {

		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(TABLE_NAME, ID + " = ?",
				new String[] { String.valueOf(informdata.getId()) });

		db.close();

		Log.d("deletedInform", informdata.toString());

	}
	public void deleteAll() {

		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(TABLE_NAME, null,	null);

		db.close();

		Log.d("deletedInform", "");

	}

}
