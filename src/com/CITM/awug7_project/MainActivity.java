package com.CITM.awug7_project;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;



public class MainActivity extends ActionBarActivity implements AnimationListener {
	
	public static int categoria = 0;
	TextView txtTitulo;
	private String[] opcionesMenu;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    
	//ACTIONBAR ------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	// Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        setUpDrawerToggle();
        return super.onCreateOptionsMenu(menu);
    }
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			if (mDrawerToggle.onOptionsItemSelected(item)) { //BOT�N IZQUIERDA
		          return true;
		        }
		    switch (item.getItemId()) {
		        case R.id.action_informe: //BOT�N IZQUIERDA INFORME
		            Intent myIntent = new Intent(getApplicationContext(),
							Inform.class);
					startActivity(myIntent);
		            return true;
		        case R.id.action_settings: //BOT�N IZQUIERDA SETTINGS
		            Intent miIntent = new Intent(getApplicationContext(),
							OpcionesActivity.class);
					startActivity(miIntent);
		            return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
		
	//---BOT�N QUE ACTIVA EL DRAWER (MEN� IZQUIERDA)-------------------
		private void setUpDrawerToggle(){
		    ActionBar actionBar = getSupportActionBar();
		    actionBar.setDisplayHomeAsUpEnabled(true);
		    actionBar.setHomeButtonEnabled(true);

		    // ActionBarDrawerToggle ties together the the proper interactions
		    // between the navigation drawer and the action bar app icon.
		    mDrawerToggle = new ActionBarDrawerToggle(
		            this,                             /* host Activity */
		            drawerLayout,                    /* DrawerLayout object */
		            R.drawable.lines,             /* nav drawer image to replace 'Up' caret */
		            R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
		            R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
		    ) {
		       /* @Override
		        public void onDrawerClosed(View drawerView) {
		            invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
		        }

		        @Override
		        public void onDrawerOpened(View drawerView) {
		            invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
		        }*/
		    };

		    // Defer code dependent on restoration of previous instance state.
		    // NB: required for the drawer indicator to show up!
		    drawerLayout.post(new Runnable() {
		        @Override
		        public void run() {
		            mDrawerToggle.syncState();
		        }
		    });

		    drawerLayout.setDrawerListener(mDrawerToggle);
		}	
	//--------------------------------------------------------------
		
	//ONCREATE	
	@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_main);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		//CREACI�N DEL MEN�
			opcionesMenu = new String[] {"Inicio", "Settings", "Gr�ficas", "Informe"};
	        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	        drawerList = (ListView) findViewById(R.id.left_drawer);
	        drawerList.setAdapter(new ArrayAdapter<String>(
	        		
	                getSupportActionBar().getThemedContext(),
	            android.R.layout.simple_list_item_1, opcionesMenu));
	        
		final LinearLayout ll_i = (LinearLayout) findViewById(R.id.subingresslayout);
		final LinearLayout ll_s = (LinearLayout) findViewById(R.id.subspendinglayout);
		
		ll_i.setVisibility(View.GONE);
		ll_s.setVisibility(View.GONE);
		
		final Animation slideDown = AnimationUtils.loadAnimation(this,
				R.anim.slide_down);
		final Animation slideUp = AnimationUtils.loadAnimation(this,
				R.anim.slide_up);
		slideUp.setAnimationListener(this);
		Button ingress_button = (Button) findViewById(R.id.ingress_but);
		Button spending_button = (Button) findViewById(R.id.spending_but);
		
		spending_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ll_s.isShown()) {
					ll_s.startAnimation(slideUp);
				} else {
					SharedPreferences preferences = PreferenceManager
			                .getDefaultSharedPreferences(ll_s.getContext());
					TextView cuadromoneda = (TextView) ll_s.findViewById(R.id.textmoneda);

					cuadromoneda.setText(preferences.getString("moneda", ""));
					ll_s.setVisibility(View.VISIBLE);
					ll_s.startAnimation(slideDown);
				}
			}
		});
		
		ingress_button.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				if (ll_i.isShown()) {
					ll_i.startAnimation(slideUp);
				} else {
					SharedPreferences preferences = PreferenceManager
			                .getDefaultSharedPreferences(ll_i.getContext());
					TextView cuadromoneda = (TextView) ll_i.findViewById(R.id.textmoneda2);

					cuadromoneda.setText(preferences.getString("moneda", ""));
					ll_i.setVisibility(View.VISIBLE);
					ll_i.startAnimation(slideDown);
				}
			}
		});
		
		//DETERMINAR ACCIONES PARA CADA ITEM DEL MEN� DRAWER
		drawerList.setOnItemClickListener(new OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView parent, View view,
	                int position, long id) {
	 
	            Fragment fragment = null;
	 
	            switch (position) {
		            case 0:
	                    fragment = new Fragment();
	                    android.support.v4.app.FragmentManager fragmentManager =getSupportFragmentManager();
	    	            fragmentManager.beginTransaction()
	    	                .replace(R.id.content_frame, fragment)
	    	                .commit();
	                    break;
	                case 1:
	                	Intent miIntent = new Intent(getApplicationContext(),
								OpcionesActivity.class);
						startActivity(miIntent);
	                    break;
	                case 2:
	                	Intent maIntent = new Intent(getApplicationContext(),
								Graficas.class);
						startActivity(maIntent);
	                    break;
	                case 3:
	                	Intent masIntent = new Intent(getApplicationContext(),
								Inform.class);
						startActivity(masIntent);
	                    break;
	            }
	            
	            drawerList.setItemChecked(position, true);
	 
	            String tituloSeccion = opcionesMenu[position];
	            getSupportActionBar().setTitle(tituloSeccion);
	 
	            drawerLayout.closeDrawer(drawerList);
	        }
	    });

	}
	//ANIMACIONES
		@Override
		public void onAnimationEnd(Animation animation) {
			Log.i("CLICK", "Animation: "+animation.toString());
			LinearLayout ll = (LinearLayout) findViewById(R.id.subingresslayout);
			LinearLayout ll_s = (LinearLayout) findViewById(R.id.subspendinglayout);
			ll_s.setVisibility(View.GONE);
			ll.setVisibility(View.GONE);	
		}
	
	//CATEGORIAS
		public void category(View v){
			ArrayAdapter<String> adapter;
			String[] arrayCategorias = getResources().getStringArray(R.array.categorias);
			LinearLayout menucategories = (LinearLayout) this.findViewById(R.id.menucategories);
	
			String IdAsString = v.getResources().getResourceName(v.getId());	
			categoria = Integer.parseInt(IdAsString.substring(IdAsString.lastIndexOf("y")+1));

			getSupportActionBar().setTitle(arrayCategorias[categoria-1]);
			Log.v("", "categoria: "+ categoria);
			DrawMiniGrafica minigrafica= new DrawMiniGrafica(getBaseContext());
			View drawminigrafica = findViewById (R.id.grafica_mini);
			drawminigrafica.invalidate();
		    
		    
			int childcount = menucategories.getChildCount();
			for (int i=0; i < childcount; i++){
			      View botoCategoria = menucategories.getChildAt(i);
			      if(v.getId()==botoCategoria.getId()){
			    	  botoCategoria.setBackgroundColor(Color.parseColor("#09cc8d"));
			      }else{
			    	  botoCategoria.setBackgroundColor(Color.parseColor("#0A6BBE"));
			      }
			};
		}


		//ESTADOS APP		
		public void onStart() {
			super.onStart();
		}
	
		public void onResume() {
			super.onResume();
		}
	
		public void onPause() {
			super.onPause();
		}
	
		public void onStop() {
			super.onStop();
		}
	
		public void onRestart() {
			super.onRestart();
		}
	
		public void onDestroy() {
			super.onDestroy();
		}
	
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

}
