package com.CITM.awug7_project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SubSpending extends Fragment {

	DBHelper dbhelper2;
	SharedPreferences preferences;
	View view;

	// int category=MainActivity.categoria;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Log.d("info", "Fragment subSpending");

		view = inflater.inflate(R.layout.subspending, container, false);
		Button spend = (Button) view.findViewById(R.id.make_spending_button);
		Log.v("", "category seleccionada: " + MainActivity.categoria);
		preferences = PreferenceManager.getDefaultSharedPreferences(view
				.getContext());
		dbhelper2 = new DBHelper(view.getContext());
		TextView pres = (TextView) view.findViewById(R.id.presupuesto);
		pres.setText(preferences.getString("presupuesto", ""));

		TextView cuadromoneda = (TextView) view.findViewById(R.id.textmoneda);
		cuadromoneda.setText(preferences.getString("moneda", ""));
		String money = preferences.getString("moneda", "");
		Log.d("moneda ------>", money);

		TextView mequedan = (TextView) view.findViewById(R.id.mequedan);
		Integer gastos = dbhelper2.getTotalgastos();
		Integer total = Integer.parseInt(preferences.getString("presupuesto",
				"100"));
		mequedan.setText((total - gastos + ""));
		EditText cost = (EditText) view.findViewById(R.id.quantitySpend);
		cost.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				/*
				 * i++; tv.setText(String.valueOf(i) + " / " +
				 * String.valueOf(charCounts));
				 */
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				TextView mequedan = (TextView) view.findViewById(R.id.mequedan);
				if (s.length()!=0) {
					Log.v("SSSSSSSSS", "asd "+s.toString());
					Integer gastos = dbhelper2.getTotalgastos();
					Integer va_a_gastar = Integer.parseInt(s.toString());
					Integer total = Integer.parseInt(preferences.getString(
							"presupuesto", "100")) - gastos - va_a_gastar;

					mequedan.setText(total.toString());
				} else {
					mequedan.setText(preferences
							.getString("presupuesto", "100"));
				}
			}
		});
		spend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				EditText concept = (EditText) getView().findViewById(
						R.id.conceptSpend);
				EditText cost = (EditText) getView().findViewById(
						R.id.quantitySpend);
				Integer cat = MainActivity.categoria;
				if (cat != 0) {
					if (concept.getText() != null && cost.getText() != null) {
						String conceptText = concept.getText().toString();
						String costText = cost.getText().toString();
						float costValue = Float.parseFloat(costText);
						DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
						Date today = Calendar.getInstance().getTime();
						String reportDate = df.format(today);

						// Toast para indicar el gasto
						Toast toast = Toast.makeText(v.getContext(),
								R.string.toastCost/* +" cat:"+cat */,
								Toast.LENGTH_LONG);
						toast.setGravity(Gravity.TOP
								| Gravity.CENTER_HORIZONTAL, 0, 150);
						toast.show();

						InformData infromdata = new InformData("cost",
								MainActivity.categoria - 1, conceptText,
								costValue, reportDate);
						dbhelper2 = new DBHelper(v.getContext());
						dbhelper2.addInform(infromdata);
						concept.setText("");
						cost.setText("");
					}
				} else {
					Toast toast = Toast.makeText(v.getContext(),
							R.string.toastCost_fail/* +" cat:"+cat */,
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL,
							0, 150);
					toast.show();
				}
			}
		});
		return view;
	}
}
