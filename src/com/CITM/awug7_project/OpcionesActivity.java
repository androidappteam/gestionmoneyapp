/*
 * Nombre: OpcionesActivity
 * Funci�n: Mostrar el men� de preferencias a trav�s de PreferenceActivity
 */
package com.CITM.awug7_project;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class OpcionesActivity extends PreferenceActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState){

		super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.options);
			
	}
	
	public void borrarTodo(final View v){
		final Context context = v.getContext();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Your Message");

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
           public void onClick(DialogInterface dialog, int id) {
                    Log.i("Dialogos", "Confirmacion Cancelada.");
                    dialog.cancel();
               }
           })
        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
            public void onClick(DialogInterface dialog, int id) {
                Log.i("Dialogos", "Confirmacion Aceptada.");
                DBHelper dbhelper = new DBHelper(context);
				dbhelper.deleteAll();
				Toast toast = Toast.makeText(v.getContext(),
						"Gatos borrados"/*+" cat:"+cat*/, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 400);
				toast.show();
                    dialog.cancel();
               }
           });

        builder.show();

	}
}
